package it.unipv.lending.test;


import static org.junit.Assert.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.*;
import org.junit.runner.RunWith;

import it.unipv.lending.service.*;
import it.unipv.lending.model.*;
import it.unipv.lending.view.*;
import junit.framework.Assert;
import it.unipv.lending.utility.*;
import org.mockito.*;


@RunWith(Arquillian.class)
public class SimpleTest extends ArquillianTest {

	
	  @Inject ProductRepository ProductRepository;
	  @Inject UserRepository    UserRepository;
	  
	

//    @Before
//    public void setUp() {
//        
//    	EntityManager entityM;
//        entityM = Mockito.mock(EntityManager.class);
//        UserRepository.setEM(entityM); // inject our stubbed entity manager
//    }

	@Test
	
	public void insertUserTest() {
		
		
		User UserTest=new User();
		
		
		final int userId=10;
		final String name="Mario";
		final String surname="Rossi";
		final String email="user@domain.com";
		final String username="foobar";
		final String city="Milano";
		final String password=Utility.hashSha256("password");
		
		
		UserTest.setName(name);
		UserTest.setSurname(surname);
		UserTest.setEmail(email);
		UserTest.setUsername(username);
		UserTest.setCity(city);
		UserTest.setPassword(password);
		UserTest.setUserId(userId);
		
		
		UserRepository.saveUser(UserTest);
		User UserRetrieve= UserRepository.getUserById(userId);
		
		Assert.assertEquals(UserTest,UserRetrieve);
	}

}
