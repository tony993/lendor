package it.unipv.lending.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.*;

import it.unipv.lending.model.LeaseContract;
import it.unipv.lending.model.User;



@Stateless
@LocalBean
public class ContractRepository {

	@PersistenceContext
	EntityManager em;
	
	@Inject
	ProductRepository productRepository;
	
	//Get all contract in the database (for testing)
	public List<LeaseContract> getAllContracts(){
		
		return em.createQuery("Select c from LeaseContract c",LeaseContract.class).getResultList();
		
	}
	
	public void setEM(EntityManager em){
		
		this.em=em;
	}
	
	public void makeContract(LeaseContract contract,long lenderId){
		
		//Set renter user in current session
		
		User renter = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("userLogged");
		
		contract.setRenter(renter);
		
		//Set Lender user after query by Product id
		contract.setLender(this.productRepository.searchSingleObjectById(lenderId).getUser());
		
		System.out.println(contract);
		em.persist(contract);
		
	}
}
