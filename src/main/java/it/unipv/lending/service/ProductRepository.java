package it.unipv.lending.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import it.unipv.lending.model.Product;
import it.unipv.lending.model.User;


@Stateless
@LocalBean
public class ProductRepository {

	@PersistenceContext
	EntityManager em;

	// Add a product in to the database
	public void saveProduct(Product p) {
		em.persist(p);
	}


	// Get all the Product in the Product Entity
	public List<Product> searchAllObjects() {
		return em.createQuery("select p from Product p", Product.class).getResultList();
	}

	// Search products by Id or Name
	public List<Product> searchObjectByName(String searchText) {

		return em.createQuery("select p from Product p where p.name= :searchText", Product.class)
				.setParameter("searchText", searchText).getResultList();
	}

	public List<Product> searchObjectById(long l) {

		return em.createQuery("select p from Product p where p.productId= :searchId", Product.class)
				.setParameter("searchId", l).getResultList();
	}
	
	public Product searchSingleObjectById(long l){
		
		return em.createQuery("select p from Product p where p.productId= :searchId", Product.class)
				.setParameter("searchId", l).getSingleResult();
	}

	// adding a product to an existing user
	public void addProduct(Product product) {

		User userLogged = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("userLogged");
		
		product.setUser(userLogged);

		userLogged.getProducts().add(product);

//		em.merge(userLogged);
		em.persist(product);

	}

	// delete of a product
	public void deleteProduct(Product product) {
		User userLogged = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("userLogged");

	

		userLogged.getProducts().remove(product);

		
		em.createNativeQuery("delete from Product where productId= :id")
		.setParameter("id", product.getProductId()).executeUpdate();


		



	}

	// Filter of the products of the UserLogged

	public List<Product> getProductusers() {
		User userLogged = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.get("userLogged");

		System.out.println("Nome loggato in getProdottiutente è: " + userLogged.getUsername());

		List<Product> productusers = userLogged.getProducts();
		System.out.println("prodotti");
		for (Product product : productusers) {
			System.out.println("name:" + product.getName());
		}
		return userLogged.getProducts();

	}

}