package it.unipv.lending.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.jboss.resteasy.annotations.cache.Cache;

import it.unipv.lending.model.Product;
import it.unipv.lending.model.User;


//@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Stateless
@LocalBean
public class UserRepository {

	@PersistenceContext
	EntityManager em;

	public void saveUser(User u) {
		em.persist(u);
	}

	// Get all the Product in the Product Entity
	public List<User> findAll() {
		return em.createQuery("select u from User u", User.class).getResultList();
	}

	// For Login
	public User getUserbyUsername(String username, String password) {
		return em.createQuery("select u from User u where u.username= :username and u.password=:password", User.class)
				.setParameter("username", username).setParameter("password", password).getSingleResult();

	}
	
	// validation for registration
	public boolean usernameExists(String username) {
		try {
		return em.createQuery("select u from User u where u.username = :username")
				.setParameter("username", username).getSingleResult() != null;
		} catch (NoResultException nre) {
			return false;
		}
	}
	
	//Simple query by Id
	public User getUserById(long id){
		
		
		return em.createQuery("select * from User where User.userId= :id",User.class)
				.setParameter("id",id).getSingleResult();
	}
	
	//Set entity manager (for testing)
	
	public void setEM(EntityManager em){
		
		this.em=em;
	}

}