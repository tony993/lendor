package it.unipv.lending.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

import it.unipv.lending.utility.*;


@Entity
public class LeaseContract implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long leaseContractId;
	private String name;
	private String description;
	private Date beginDate;
	private Date endDate;
	

	private Duration duration;
    private double billingAmount;
	private boolean insurance;
	private boolean advancedPayment;
	private boolean prematureTermination;
	
	//Relationship attributes
	
	@ManyToOne
	@JoinColumn(name = "lender_fk")
	private User lender;
	
	@ManyToOne
	@JoinColumn(name = "renter_fk")
	private User renter;
	
	@OneToOne
	@JoinColumn(name="product_fk")
	private Product product;
	
	
	public Product getProduct(){
		
		return this.product;
	}
	
	
	public User getRenter() {
		return renter;
	}

	public void setRenter(User renter) {
		this.renter = renter;
	}

	public User getLender() {
		return lender;
	}

	public void setLender(User lender) {
		this.lender = lender;
	}

	public LeaseContract() {
		super();
	}

	public long getLeaseContractId() {
		return leaseContractId;
	}

	public void setLeaseContractId(long leaseContractId) {
		this.leaseContractId = leaseContractId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public double getBillingAmount() {
		return billingAmount;
	}

	public void setBillingAmount(double billingAmount) {
		this.billingAmount = billingAmount;
	}


	public boolean isInsurance() {
		return insurance;
	}


	public void setInsurance(boolean insurance) {
		this.insurance = insurance;
	}


	public boolean isAdvancedPayment() {
		return advancedPayment;
	}


	public void setAdvancedPayment(boolean advancedPayment) {
		this.advancedPayment = advancedPayment;
	}
	
	public String toString(){
		
		return this.name+this.description+this.billingAmount+this.renter+this.lender;
	}

}
