package it.unipv.lending.model;

import java.awt.Image;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;


@Entity
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long productId;

	private String name;
	private String description;
	@Lob
	@Column(name="image")
	private Image image;
	private boolean available;

	//Relationship attributes
	
	@ManyToOne
	@JoinColumn(name = "user_fk")
	private User user;
	
	@OneToOne(mappedBy= "product",cascade=CascadeType.ALL,orphanRemoval=true)
	private LeaseContract contract;
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isAvailable() {
		return available;
	}

	public Product() {
		super();
		
		this.setAvailable(false);
		this.contract=null;
	}
	
	public Product(String nam,String desc){
		
		super();
		
		this.name=nam;
		this.description=desc;
		this.available=false;
		this.contract=null;
	}
	
	public Product(String nam,String desc,Image img){
		
        super();
		
		this.name=nam;
		this.description=desc;
		this.image=img;
		this.available=false;
		this.contract=null;
	}

	// Direct Product constructor (for testing)
	public Product(String n, String desc, boolean ava) {
		this.name = n;
		this.description = desc;
		this.available = ava;

	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAvailable(boolean a) {

		this.available = a;
	}
	
	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String toString(){
		
	  return (this.getProductId()+this.getName()+this.getDescription());
	}

}
