package it.unipv.lending.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import it.unipv.lending.model.*;
import it.unipv.lending.service.*;
import javax.enterprise.context.*;
import javax.faces.context.FacesContext;

@Named("productView")
@RequestScoped
public class ProductView implements Serializable {

	
	private static final long serialVersionUID = 1L;

	
	private List<Product> products;
	private String searchText,searchType;
	
	private long IdToRent;
	private Product productToRent;

    @Inject
	private ProductRepository productService;
  
	
	@PostConstruct		
	public void init(){
		
		products =productService.searchAllObjects();
		
	}

	public List<Product> searchObjectList(){
	
    
		switch(searchType){
      
    	 
		case "Name":{  this.products=this.productService.searchObjectByName(searchText);
    	  return products;
    	  
		}
     
		case "Id": {  this.products=this.productService.searchObjectById(Long.parseLong(searchText));
    	  return this.products;
    	  
		}
		default : {
			
			return this.products;
		}
	  }
	}
	
	public void rentProduct(){
		
		
	}
	
	public List<Product> getProducts(){
		return products;
	}
	
	public String getSearchType() {
		return searchType;
	}


	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}


	public String getSearchText() {
		return searchText;
	}


	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	
	public long getIdToRent() {
		return IdToRent;
	}

	public void setIdToRent(long idToRent) {
		IdToRent = idToRent;
	}

	

	public Product getProductToRent() {
		return productToRent;
	}

	public void setProductToRent(Product productToRent) {
		this.productToRent = productToRent;
	}

	public String goToAddProduct(){
		return "addProduct.xhtml?faces-redirect=true";
	}
	
	public String goToUserMenu(){
		
		return "userMenu.xhtml?faces-redirect=true";
	}
	
	//Loopback from product renting
	
	public String goToProductList(){
		
		return "productsList.xhtml?faces-redirect=true";
	}
	
   public String goToProductRent(long id){
	  
	   //Initaliaze product to rent display
	   
	  
	   setIdToRent(id);
	   this.setProductToRent(productService.searchSingleObjectById(this.getIdToRent()));
	   
	   RequestContext.getCurrentInstance().update("ObjectRenting");
	   return "makeContract.xhtml?faces-redirect=true";
   }
   
   public String goToDeleteProduct() {
		return "deleteProduct.xhtml?faces-redirect=true";
	}

}
