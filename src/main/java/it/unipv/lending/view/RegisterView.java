package it.unipv.lending.view;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import it.unipv.lending.model.User;
import it.unipv.lending.service.UserRepository;
import it.unipv.lending.utility.Utility;;

@Named("registerView")
@RequestScoped
public class RegisterView implements Serializable {

	private static final long serialVersionUID = 1L;

	//Registration attributes
	
	private String name;
	private String surname;
	private String username;
	private String email;
	private String confirmEmail;
	private String password;
	private String confirmPassword;
		

	private String city;

	@Inject
	private UserRepository userRepository;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getConfirmEmail() {
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	// do the registration part
	public String register() {
		clearMessages();
		
		if (checkIfEmpty()) {
			return "registration.xhtml";
		}
		
		if (!email.equals(confirmEmail)) {
			FacesContext.getCurrentInstance().addMessage("registrationForm:confirm_email", new FacesMessage("Email addresses do not match."));
			
			return "registration.xhtml";
		}
		if(!password.equals(confirmPassword)){
			
			FacesContext.getCurrentInstance().addMessage("registrationForm:confirm_password", new FacesMessage("Passwords do not match."));
			
			return "registration.xhtml";
		}
		
		if (userRepository.usernameExists(username)) {
			FacesContext.getCurrentInstance().addMessage("registrationForm:username", new FacesMessage("Username exists, choose another one."));
			
			return "registration.xhtml";
		}
		
		// need to check password format
		
		User user = new User();
		user.setName(name);
		user.setSurname(surname);
		user.setUsername(username);
		user.setEmail(email);
		user.setPassword(Utility.hashSha256(password));
		user.setCity(city);
		userRepository.saveUser(user);

		return "login.xhtml?faces-redirect=true";

	}
	
	private void clearMessages() {
		Iterator<String> itIds = FacesContext.getCurrentInstance().getClientIdsWithMessages();
		while (itIds.hasNext()) {
		    List<FacesMessage> messageList = FacesContext.getCurrentInstance().getMessageList(itIds.next());
		    if (!messageList.isEmpty()) { // if empty, it will be unmodifiable and throw UnsupportedOperationException...
		        messageList.clear();
		    }
		}
	}
	
	private boolean checkIfEmpty() {
		boolean isAnyEmpty = false;
		
		UIViewRoot viewRoot =  FacesContext.getCurrentInstance().getViewRoot();
	    UIComponent component1 = viewRoot.findComponent("registrationForm");
	    HtmlForm form = (HtmlForm) component1;
	    
	    List<UIComponent> components = form.getChildren();
		
		for (UIComponent component : components) {
			if ((component instanceof HtmlInputText) || (component instanceof HtmlInputSecret)) {
				if (((UIInput) component).getValue().toString().isEmpty()) {
					FacesContext.getCurrentInstance().addMessage(component.getClientId(), 
							new FacesMessage("Field cannot be empty."));
					
					isAnyEmpty = true;
				}
			}
		}
		
		return isAnyEmpty;
	}

	public String goToLogin() {
		return "login.xhtml?faces-redirect=true";
	}
	
	public String goToRegistration(){
		return "registration.xhtml?faces-redirect=true";
	}
}
