package it.unipv.lending.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import it.unipv.lending.model.LeaseContract;
import it.unipv.lending.model.Product;
import it.unipv.lending.service.ContractRepository;
import it.unipv.lending.service.ProductRepository;

@Named
@RequestScoped

public class ContractView {
	
	 @Inject 
	private ContractRepository service;
	
	private static final long serialVersionUID = 1L;
	private List<LeaseContract> contracts;

	
	@PostConstruct
	public void init(){
		this.contracts=service.getAllContracts();
		
	}
	
	public List<LeaseContract> getContracts(){
		return this.contracts;
	}
	
	public void rentObject(){
		
		
	}
	
	public String goToUserMenu(){
		
		return "userMenu.xhtml?faces-redirect-true";
	}
	
	public String goToProductList(){
		
		return "productsList.xhtml?faces-redirect-true";
	}
	
	

}
