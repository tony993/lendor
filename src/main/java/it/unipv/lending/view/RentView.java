package it.unipv.lending.view;

import javax.inject.*;
import it.unipv.lending.utility.*;
import it.unipv.lending.model.*;
import it.unipv.lending.service.*;
import java.util.Date;
import javax.enterprise.context.*;

@Named("rentView")
@RequestScoped
public class RentView {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String description;
	private Date beginDate;
	private Date endDate;
	private long idToRent;
	

	private Duration duration;
    private double billingAmount;
	private boolean insurance;
	private boolean advancedPayment;
	
	@Inject
	ContractRepository service;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public double getBillingAmount() {
		return billingAmount;
	}

	public void setBillingAmount(double billingAmount) {
		this.billingAmount = billingAmount;
	}

	public boolean isInsurance() {
		return insurance;
	}

	public void setInsurance(boolean insurance) {
		this.insurance = insurance;
	}

	public boolean isAdvancedPayment() {
		return advancedPayment;
	}

	public void setAdvancedPayment(boolean advancedPayment) {
		this.advancedPayment = advancedPayment;
	}
	
	public long getIdToRent() {
		return idToRent;
	}

	public void setIdToRent(long idToRent) {
		this.idToRent = idToRent;
	}

	public String makeContract(){
		
		
		LeaseContract contract=new LeaseContract();
		
		System.out.println("idtoRent: "+this.idToRent);
		//Bean data insertion
		
		contract.setName(this.name);
		contract.setDescription(description);
		contract.setBeginDate(this.beginDate);
		contract.setEndDate(this.endDate);
		contract.setBillingAmount(this.billingAmount);
		contract.setAdvancedPayment(this.advancedPayment);
		contract.setInsurance(this.insurance);
	
		
		//Duration generation
		
		service.makeContract(contract,this.idToRent);
		
		return "productLists.xhtml?faces-redirect=true";
	}
	
	
}
