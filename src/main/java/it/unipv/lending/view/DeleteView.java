package it.unipv.lending.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import it.unipv.lending.model.Product;
import it.unipv.lending.service.ProductRepository;

@Named
@RequestScoped
public class DeleteView implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ProductRepository service;

	private List<Product> productusers;

	@PostConstruct
	public void init() {
		productusers = service.getProductusers();
	}

	public List<Product> getProductusers() {
		return productusers;
	}

	public void deleteProduct(Product product) {
		service.deleteProduct(product);

	}

	 public String goToProductList() {
	 return "productsList.xhtml?faces-redirect=true";
	 }

}